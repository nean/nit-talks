<?php

namespace NitTalks\Twig;

use Twig_Extension;
use Symfony\Component\HttpFoundation\Request;

class NitTalksExtension extends Twig_Extension
{
    private $request;

    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    public function getFunctions()
    {
        return array(
            'asset'    => new \Twig_Function_Method($this, 'asset'),
        );
    }

    public function asset($url)
    {
        return sprintf(
            '%s://%s/%s',
            $this->request->getScheme(),
            $this->request->getHost().$this->request->getBasePath(),
            ltrim($url, '/')
        );
    }

    public function getName()
    {
        return 'nittalks';
    }
}