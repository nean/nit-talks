<?php

namespace NitTalks;

use Silex\Application as SilexApplication;

class Application extends SilexApplication
{
    public function __construct()
    {
        parent::__construct();
    }
}
