<?php

namespace NitTalks\Repository;

class NitRepository
{
	private $db;

	public function __construct($db)
	{
		$this->db = $db;
	}

	public function findOneById($id)
	{
		$person = $this->db->fetchAssoc(
			'SELECT * FROM nits WHERE id = :id',
			array('id' => (int) $id)
		);

		return $person;
	}

	public function findAll()
	{
		$query = $this->db->createQueryBuilder()
			->select('*')
			->from('nits','n');

		$result = $query->execute()->fetchAll();
		return $result;
	}

	public function findOneByShort($short)
	{
		$query = $this->db->createQueryBuilder()
			->select('*')
			->from('nits', 'n')
			->where('n.short = :short')
			->setParameter(':short' , $short);

		$result = $query->execute()->fetchAll();
		return $result[0];
	}

	public function findOneByLocation($location)
	{
		$query = $this->db->createQueryBuilder()
			->select('*')
			->from('nits', 'n')
			->where('n.location = :location')
			->setParameter(':location' , $location);

		$result = $query->execute()->fetchAll();
		return $result[0];
	}

	public function getDb()
	{
		return $this->db;
	}
}