<?php

namespace NitTalks\Repository;

class PersonRepository
{
	private $db;

	public function __construct($db)
	{
		$this->db = $db;
	}

	public function findOneById($id)
	{
		$person = $this->db->fetchAssoc(
			'SELECT * FROM persons WHERE id = :id',
			array('id' => (int) $id)
		);

		return $person;
	}

	public function findAll($offset = 0,$max = 10)
	{
		$query = $this->db->createQueryBuilder()
			->select('*')
			->from('persons','p')
			->setMaxResults($max)
			->setFirstResult($offset);

		$result = $query->execute()->fetchAll();
		return $result;
	}

	public function findAllByNitId($nitId, $offset = 0, $max = 10)
	{
		$query = $this->db->createQueryBuilder()
			->select('*')
			->from('persons', 'p')
			->where('p.nit_id = :nit_id')
			->setParameter(':nit_id' , $nitId)
			->setMaxResults($max)
			->setFirstResult($offset);

		$result = $query->execute()->fetchAll();
		return $result;
	}

	public function getDb()
	{
		return $this->db;
	}
}