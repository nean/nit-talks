<?php

namespace NitTalks\Repository;

class VideoRepository
{
	private $db;

	public function __construct($db)
	{
		$this->db = $db;
	}

	public function findOneById($id)
	{
		$video = $this->db->fetchAssoc(
			'SELECT * FROM videos WHERE id = :id',
			array('id' => (int) $id)
		);

		return $video;
	}

	public function findAll($offset = 0,$max = 10)
	{
		$query = $this->db->createQueryBuilder()
			->select('*')
			->from('videos','v')
			->setMaxResults($max)
			->setFirstResult($offset);

		$result = $query->execute()->fetchAll();
		return $result;
	}

	public function findAllByPersonId($personId, $offset = 0, $max = 10)
	{
		$query = $this->db->createQueryBuilder()
			->select('*')
			->from('videos','v')
			->where('v.person_id = :person_id')
			->setParameter(':person_id', $personId)
			->setMaxResults($max)
			->setFirstResult($offset);

		$result = $query->execute()->fetchAll();
		return $result;
	}

	public function findAllByNitId($nitId, $offset = 0, $max = 10)
	{
		$query = $this->db->createQueryBuilder()
			->select('*')
			->from('videos','v')
			->where('v.nit_id = :nit_id')
			->setParameter(':nit_id', $nitId)
			->setMaxResults($max)
			->setFirstResult($offset);

		$result = $query->execute()->fetchAll();
		return $result;
	}

	public function getDb()
	{
		return $this->db;
	}
}