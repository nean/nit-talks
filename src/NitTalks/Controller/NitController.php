<?php

namespace NitTalks\Controller;

class NitController
{
	private $repo;

	public function __construct($repository, $app)
	{
		$this->repo = $repository;
		$this->app = $app;
	}

	public function index()
	{
		$nits = $this->repo->findAll();
		return $this->app['twig']->render('nit/list.twig', array(
			'nits' => $nits,
			'active' => 'nits'
		));
	}

	public function viewByLocation($location)
	{
		$nit = $this->repo->findOneByLocation($location);
		return $this->app['twig']->render('dump.twig', array(
			'nit' => $nit,
		));
	}

	public function viewPersons($location)
	{
		$nit = $this->repo->findOneByLocation($location);

		if (!$nit) {
			echo "nit doesnt exist !";
			exit();
		}

		$nitId = $nit['id'];
		$persons = $this->app['person.repository']->findAllByNitId($nitId);
		return $this->app['twig']->render('person/list.twig', array(
			'persons' => $persons,
		));
	}

	public function viewVideos($location)
	{
		$nit = $this->repo->findOneByLocation($location);

		if (!$nit) {
			echo "nit doesnt exist !";
			exit();
		}

		$nitId = $nit['id'];
		$videos = $this->app['video.repository']->findAllByNitId($nitId);
		return $this->app['twig']->render('video/list.twig', array(
			'videos' => $videos,
		));
	}
}