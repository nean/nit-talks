<?php

namespace NitTalks\Controller;

use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class PersonController
{
	private $repo;

	public function __construct($repository, $app)
	{
		$this->repo = $repository;
		$this->app = $app;
	}

	public function index()
	{
		$persons = $this->repo->findAll();
		return $this->app['twig']->render('person/list.twig', array(
			'persons' => $persons,
			'active' => 'persons'
		));
	}

	public function profile($id)
	{
		$person = $this->repo->findOneById($id);
		if(false === $person) {
			throw new NotFoundHttpException('Person Not Found !');
		}
		$nit = $this->app['nit.repository']->findOneById($person['nit_id']);
		$videos = $this->app['video.repository']->findAllByPersonId($id,0,3);
		return $this->app['twig']->render('person/profile.twig', array(
			'person' => $person,
			'nit' => $nit,
			'videos' => $videos,
		));
	}

	public function viewVideos($id)
	{
		$person = $this->repo->findOneById($id);
		if(!$person) {
			echo "person doesnt exist";
			exit();
		}

		$personId = $person['id'];
		$videos = $this->app['video.repository']->findAllByPersonId($personId);
		return $this->app['twig']->render('video/list.twig', array(
			'videos' => $videos,
		));
	}
}