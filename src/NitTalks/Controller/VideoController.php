<?php

namespace NitTalks\Controller;

class VideoController
{
	private $repo;

	public function __construct($repository, $app)
	{
		$this->repo = $repository;
		$this->app = $app;
	}

	public function index()
	{
		$videos = $this->repo->findAll();
		return $this->app['twig']->render('video/list.twig', array(
			'videos' => $videos,
			'active' => 'videos'
		));
	}

	public function watch($id)
	{
		$video = $this->repo->findOneById($id);
		return $this->app['twig']->render('video/watch.twig', array(
			'video' => $video,
		));
	}
}