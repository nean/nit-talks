<?php

require_once __DIR__.'/../vendor/autoload.php';

$app = new NitTalks\Application();

return $app;