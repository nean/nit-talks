<?php

$app = require __DIR__.'/bootstrap.php';

$app['debug'] = true;

$app->register(new Silex\Provider\TwigServiceProvider(), array(
    'twig.path' => __DIR__.'/templates',
    'twig.options' => array('cache' => __DIR__.'/cache/twig'),
));

$app['twig'] = $app->share($app->extend('twig', function ($twig, $c) {
    $twig->addExtension(new NitTalks\Twig\NitTalksExtension($c['request']));
    return $twig;
}));

$app->register( new Silex\Provider\DoctrineServiceProvider(), array(
	'db.options' => array(
		'driver' => 'pdo_mysql',
		'host'	 => 'localhost',
		'user'	 => 'root',
		'password'=> 'root',
		'charser' => 'utf8',
		'dbname' => 'ntalks',
	),
));

$app->register(new Silex\Provider\ServiceControllerServiceProvider());

$app->register(new Silex\Provider\UrlGeneratorServiceProvider());

$app['video.controller'] = $app->share(function () use ($app) {
	return new NitTalks\Controller\VideoController($app['video.repository'], $app);
});

$app['video.repository'] = $app->share(function () use ($app) {
	return new NitTalks\Repository\VideoRepository($app['db']);
});

$app['person.controller'] = $app->share(function () use ($app) {
	return new NitTalks\Controller\PersonController($app['person.repository'], $app);
});

$app['person.repository'] = $app->share(function () use ($app) {
	return new NitTalks\Repository\PersonRepository($app['db']);
});

$app['nit.controller'] = $app->share(function () use ($app) {
	return new NitTalks\Controller\NitController($app['nit.repository'], $app);
});

$app['nit.repository'] = $app->share(function () use ($app) {
	return new NitTalks\Repository\NitRepository($app['db']);
});
return $app;