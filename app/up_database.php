<?php

$app = require __DIR__.'/config.php';

$sm = $app['db']->getSchemaManager();

$currentSchema = $sm->createSchema();

$schema = new Doctrine\DBAL\Schema\Schema();

$nitsTable = $schema->createTable('nits');
$nitsTable->addColumn('id', 'smallint')
	->setUnsigned(true)
	->setAutoincrement(1);
$nitsTable->addColumn('name', 'string');
$nitsTable->addColumn('short', 'string');
$nitsTable->addColumn('location', 'string');
$nitsTable->addColumn('desc', 'text');
$nitsTable->setPrimaryKey(array('id'));

$personsTable = $schema->createTable('persons');
$personsTable->addColumn('id','integer')
	->setUnsigned(true)
	->setAutoincrement(1);
$personsTable->addColumn('name', 'string');
$personsTable->addColumn('desc', 'text');
$personsTable->addColumn('nit_id', 'smallint')
	->setUnsigned(true);
$personsTable->addForeignKeyConstraint($nitsTable, array('nit_id'), array('id'), array('onDelete' => 'CASCADE'));
$personsTable->setPrimaryKey(array('id'));

$videosTable = $schema->createTable('videos');
$videosTable->addColumn('id', 'integer')
	->setUnsigned(true)
	->setAutoincrement(1);
$videosTable->addColumn('title', 'string');
$videosTable->addColumn('desc', 'text');
$videosTable->addColumn('person_id', 'integer')
	->setUnsigned(true);
$videosTable->addColumn('nit_id', 'smallint')
	->setUnsigned(true);
$videosTable->addColumn('path', 'string');
$videosTable->addForeignKeyConstraint($personsTable, array('person_id'), array('id'), array('onDelete' => 'CASCADE'));
$videosTable->addForeignKeyConstraint($nitsTable, array('nit_id'), array('id'), array('onDelete' => 'CASCADE'));
$videosTable->setPrimaryKey(array('id'));

$sql = $currentSchema->getMigrateToSql($schema, $app['db']->getDatabasePlatform());

var_dump($sql);


if(isset($argv[1]) && ($argv[1] == 'd' || $argv[1] == 'debug')) {
	die();
}

$app['db']->transactional(function ($conn) use ($sql) {
	foreach ($sql as $stmt) {
		$conn->exec($stmt);
	}
});

echo "done";