<?php

$app = require __DIR__.'/../app/config.php';

$app->get('/', function () use ($app) {
	return $app['twig']->render(
		'index.twig',
		array(
			'active' => 'home',
		)
	);
})
->bind('index');

$app->get('/videos', 'video.controller:index')
	->bind('videos');

$app->get('/video/{id}', 'video.controller:watch')
	->assert('id', '\d+')
	->bind('video');



$app->get('/persons', 'person.controller:index')
	->bind('persons');

$app->get('/person/{id}', 'person.controller:profile')
	->assert('id', '\d+')
	->bind('person');

$app->get('/person/{id}/videos', 'person.controller:viewVideos')
	->assert('id', '\d+')
	->bind('person.videos');



$app->get('/nits', 'nit.controller:index')
	->bind('nits');

$app->get('/nit/{location}', 'nit.controller:viewByLocation')
	->bind('nit');

$app->get('/nit/{location}/persons', 'nit.controller:viewPersons')
	->bind('nit.persons');

$app->get('/nit/{location}/videos', 'nit.controller:viewVideos')
	->bind('nit.videos');

$app->run();