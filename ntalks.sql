-- Adminer 3.6.1 MySQL dump

SET NAMES utf8;
SET foreign_key_checks = 0;
SET time_zone = 'SYSTEM';
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

DROP TABLE IF EXISTS `nits`;
CREATE TABLE `nits` (
  `id` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `short` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `desc` longtext COLLATE utf8_unicode_ci NOT NULL,
  `location` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO `nits` (`id`, `name`, `short`, `desc`, `location`) VALUES
(1,	'National Institute of Technology, Agartala',	'nita',	'		\'name\' => \'National Institute of Technology, Agartala\',\r\n		\'short\' => \'nita\',\r\n		\'location\' => \'agartala\',',	'agartala'),
(2,	'Motilal Nehru National Institute of Technology, Allahabad',	'mnnit',	'		\'name\' => \'Motilal Nehru National Institute of Technology, Allahabad\',\r\n		\'short\' => \'mnnit\',\r\n		\'location\' => \'allahabad\',',	'allahabad'),
(3,	'Maulana Azad National Institute of Technology, Bhopal',	'manit',	'		\'name\' => \'Maulana Azad National Institute of Technology, Bhopal\',\r\n		\'short\' => \'manit\',\r\n		\'location\' => \'bhopal\',',	'bhopal'),
(4,	'National Institute of Technology, Calicut',	'nitc',	'		\'name\' => \'National Institute of Technology, Calicut\',\r\n		\'short\' => \'nitc\',\r\n		\'location\' => \'calicut\',',	'calicut'),
(5,	'National Institute of Technology, Durgapur',	'nitdgp',	'		\'name\' => \'National Institute of Technology, Durgapur\',\r\n		\'short\' => \'nitdgp\',\r\n		\'location\' => \'durgapur\',',	'durgapur'),
(6,	'National Institute of Technology, Hamirpur',	'nith',	'		\'name\' => \'National Institute of Technology, Hamirpur\',\r\n		\'short\' => \'nith\',\r\n		\'location\' => \'hamirpur\',',	'hamirpur'),
(7,	'Malaviya National Institute of Technology, Jaipur',	'mnit',	'		\'name\' => \'Malaviya National Institute of Technology, Jaipur\',\r\n		\'short\' => \'mnit\',\r\n		\'location\' => \'jaipur\',',	'jaipur'),
(8,	'Dr. B R Ambedkar National Institute of Technology',	'nitj',	'		\'name\' => \'Dr. B R Ambedkar National Institute of Technology\',\r\n		\'short\' => \'nitj\',\r\n		\'location\' => \'jalandhar\',',	'jalandhar'),
(9,	'National Institute of Technology, Jamshedpur',	'nitjsr',	'		\'name\' => \'National Institute of Technology, Jamshedpur\',\r\n		\'short\' => \'nitjsr\',\r\n		\'location\' => \'jamshedpur\',',	'jamshedpur'),
(10,	'National Institute of Technology, Kurukshetra',	'nitkkr',	'		\'name\' => \'National Institute of Technology, Kurukshetra\',\r\n		\'short\' => \'nitkkr\',\r\n		\'location\' => \'kurukshetra\',',	'kurukshetra'),
(11,	'Visvesvaraya National Institute of Technology, Nagpur',	'vnit',	'		\'name\' => \'Visvesvaraya National Institute of Technology, Nagpur\',\r\n		\'short\' => \'vnit\',\r\n		\'location\' => \'nagpur\',',	'nagpur'),
(12,	'National Institute of Technology, Patna',	'nitp',	'		\'name\' => \'National Institute of Technology, Patna\',\r\n		\'short\' => \'nitp\',\r\n		\'location\' => \'patna\',',	'patna'),
(13,	'National Institute of Technology, Raipur',	'nitrr',	'		\'name\' => \'National Institute of Technology, Raipur\',\r\n		\'short\' => \'nitrr\',\r\n		\'location\' => \'raipur\',',	'raipur'),
(14,	'National Institute of Technology, Rourkela',	'nitrkl',	'		\'name\' => \'National Institute of Technology, Rourkela\',\r\n		\'short\' => \'nitrkl\',\r\n		\'location\' => \'rourkela\',',	'rourkela'),
(15,	'National Institute of Technology, Silchar',	'nits',	'		\'name\' => \'National Institute of Technology, Silchar\',\r\n		\'short\' => \'nits\',\r\n		\'location\' => \'silchar\',',	'silchar'),
(16,	'National Institute of Technology, Srinagar',	'nitsri',	'		\'name\' => \'National Institute of Technology, Srinagar\',\r\n		\'short\' => \'nitsri\',\r\n		\'location\' => \'srinagar\',',	'srinagar'),
(17,	'S V National Institute of Technology, Surat',	'svnit',	'		\'name\' => \'S V National Institute of Technology, Surat\',\r\n		\'short\' => \'svnit\',\r\n		\'location\' => \'surat\',',	'surat'),
(18,	'National Institute of Technology Karnataka',	'nitk',	'		\'name\' => \'National Institute of Technology Karnataka\',\r\n		\'short\' => \'nitk\',\r\n		\'location\' => \'surathkal\',',	'surathkal'),
(19,	'National Institute of Technology, Tiruchirappalli',	'nitt',	'		\'name\' => \'National Institute of Technology, Tiruchirappalli\',\r\n		\'short\' => \'nitt\',\r\n		\'location\' => \'trichy\',',	'trichy'),
(20,	'National Institute of Technology, Warangal',	'nitw',	'		\'name\' => \'National Institute of Technology, Warangal\',\r\n		\'short\' => \'nitw\',\r\n		\'location\' => \'warangal\',',	'warangal');

DROP TABLE IF EXISTS `persons`;
CREATE TABLE `persons` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nit_id` smallint(5) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `desc` longtext COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_A25CC7D36A43F15C` (`nit_id`),
  CONSTRAINT `FK_A25CC7D36A43F15C` FOREIGN KEY (`nit_id`) REFERENCES `nits` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO `persons` (`id`, `nit_id`, `name`, `desc`) VALUES
(1,	19,	'Neranjen',	'Its Me :P');

DROP TABLE IF EXISTS `videos`;
CREATE TABLE `videos` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `person_id` int(10) unsigned NOT NULL,
  `nit_id` smallint(5) unsigned NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `desc` longtext COLLATE utf8_unicode_ci NOT NULL,
  `path` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_29AA6432217BBB47` (`person_id`),
  KEY `IDX_29AA64326A43F15C` (`nit_id`),
  CONSTRAINT `FK_29AA6432217BBB47` FOREIGN KEY (`person_id`) REFERENCES `persons` (`id`) ON DELETE CASCADE,
  CONSTRAINT `FK_29AA64326A43F15C` FOREIGN KEY (`nit_id`) REFERENCES `nits` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO `videos` (`id`, `person_id`, `nit_id`, `title`, `desc`, `path`) VALUES
(1,	1,	19,	'Test 1',	'The greatest Video Ever made by man :D',	'upload/video.mp4'),
(2,	1,	19,	'Test 1',	'The greatest Video Ever made by man :D',	'upload/video.mp4'),
(3,	1,	19,	'Test 1',	'The greatest Video Ever made by man :D',	'upload/video.mp4');

-- 2013-01-30 00:58:25
